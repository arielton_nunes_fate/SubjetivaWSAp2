package br.edu.uniateneu.SubjetivaAp2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
@EntityScan("br.edu.uniateneu.SubjetivaAp2.modelo*")
@SpringBootApplication
public class SubjetivaAp2Application {

	public static void main(String[] args) {
		SpringApplication.run(SubjetivaAp2Application.class, args);
	}

}
